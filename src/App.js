import FolderWrapper from "./components/FolderWrapper";

function App() {
  return (
    <div className="App">
      <FolderWrapper />
    </div>
  );
}

export default App;
