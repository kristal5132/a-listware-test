import React from 'react';
import data from '../../data.json';
import Folder from "../Folder";

class FolderWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isToggledFolder: false }
        this.changeToggleFolderState = this.changeToggleFolderState.bind(this);
    }

    changeToggleFolderState() {
        this.setState({
            isToggledFolder: true
        })
    }

    // because folder do not have any ids so we need to use index, but its a bad practice
    render() {
        return data.map((folder, index) => (
        <Folder
            key={index}
            folder={folder}
            level={0}
            expandedFolders={[
                '/Common7/IDE', '/DIA SDK/bin/arm'
            ]}
            handleChangeFolderState={this.changeToggleFolderState}
            isToggledFolder={this.state.isToggledFolder}
        />
    ))
    }
}

export default FolderWrapper;
