import React from 'react';
import File from "../File";
import { getSpaces } from "../utils/getSpaces";
import { checkIfExpanded } from "../utils/checkIfExpanded";

class Folder extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: !props.isToggledFolder ?
                props.level === 0 || props.isPreviousFolderOpen ? checkIfExpanded(props.expandedFolders, props.folder.name) : false
                : false
        };

        this.handleChangeFolderVisibility = this.handleChangeFolderVisibility.bind(this);
    }

    handleChangeFolderVisibility() {
        if (this.props.handleChangeFolderState) {
            this.props.handleChangeFolderState()
        }
        this.setState((prevState) => ({
            open: !prevState.open
        }));
    }

    hasAnotherFolder() {
        return !!this.props.folder?.children
    }

    isFolder() {
        return this.props.folder.type === 'FOLDER'
    }

    getCurrentLevel() {
        return this.props.level + 1
    }

    componentDidMount() {
        if (!this.props.expandedFolders) {
            this.setState({ open: true })
        }
    }

    render() {
        return (
            <div>
                {this.isFolder() &&
                    <p className="folder-name" onClick={this.handleChangeFolderVisibility}>
                        {getSpaces(this.props.level) + (this.state.open ? '- ' : '+ ') + this.props.folder.name}
                    </p>
                }
                <div>
                    {this.hasAnotherFolder() ? this.state.open && this.props.folder.children.map((childrenFolder, index) =>
                        <div key={index}>
                            <Folder
                                expandedFolders={this.props.expandedFolders}
                                isPreviousFolderOpen={this.state.open}
                                folder={childrenFolder}
                                level={this.getCurrentLevel()}
                                isToggledFolder={this.props.isToggledFolder}
                            />
                        </div>
                        ) : <File level={this.getCurrentLevel()} name={this.props.folder.name} />
                    }
                </div>
            </div>
        )
    }
}

export default Folder;
