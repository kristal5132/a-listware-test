import React from 'react';
import { getSpaces } from "../utils/getSpaces";

class File extends React.PureComponent {
    render() {
        return (
            <p>{getSpaces(this.props.level) + this.props.name}</p>
        )
    }
}

export default File;
