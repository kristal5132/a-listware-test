export const checkIfExpanded = (paths, currentFolder) => {
    return paths?.length && !!paths.map(path => path.split('/').includes(currentFolder)).filter(Boolean).length
}
