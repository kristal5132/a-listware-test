export const getSpaces = (level) => {
    const spaces = [];
    for (let i = 0; i < level; i++) {
        spaces.push('\xa0')
    }
    return spaces.join(' ');
}
