const spiralMatrix = (matrix) => {
    if (!matrix.length) return [];

    let result = [];

    let rowStart = 0,
        rowEnd = matrix.length - 1,
        colStart = 0,
        colEnd = matrix[0].length - 1;

    while (colStart <= colEnd && rowStart <= rowEnd) {

        for (let i = colStart; i <= colEnd; i++) {
            result.push(matrix[rowStart][i]);
        }
        rowStart++;

        for (let i = rowStart; i <= rowEnd; i++) {
            result.push(matrix[i][colEnd]);
        }
        colEnd--;
        if (rowStart <= rowEnd) {
            for (let i = colEnd; i >= colStart; i--) {
                result.push(matrix[rowEnd][i]);
            }
        }
        rowEnd--;

        if (colStart <= colEnd) {
            for (let i = rowEnd; i >= rowStart; i--) {
                result.push(matrix[i][colStart]);
            }
        }
        colStart++;
    }

    return result.join(' ');
};

console.log(spiralMatrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
